$(function() {
    AOS.init();

    $('.owl-carousel').owlCarousel({
        loop:true,
        margin: 10,
        responsiveClass: true,
        navText: ['<i class="icon-angle-left"></i>', '<i class="icon-angle-right"></i>'],
        responsive: {
            0:{
                items:1,
                nav:true
            },
            600:{
                items:3,
                nav:false
            },
            1000:{
                items: 4,
                nav: true,
                loop: false
            }
        }
    });

    // Timer
    // Set the date we're counting down to
    var wedding_date  = document.querySelector('#wedding_date').value;
    var countDownDate = new Date(wedding_date).getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

      // Get today's date and time
      var now = new Date().getTime();

      // Find the distance between now and the count down date
      var distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Display the result in the element with id="demo"
      document.getElementById("demo").innerHTML = `
        <div class="row text-center">
            <div class="col-sm-3">
                <div class="countdown-box">
                    <div class="num">${days}</div>
                    <div>days</div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="countdown-box">
                    <div class="num">${hours}</div>
                    <div>hours</div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="countdown-box">
                    <div class="num">${minutes}</div>
                    <div>minutes</div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="countdown-box">
                    <div class="num">${seconds}</div>
                    <div>seconds</div>
                </div>
            </div>
        </div>
      `; // days + "d " + hours + "h " + minutes + "m " + seconds + "s ";

      // If the count down is finished, write some text
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
      }
    }, 1000);

    $(window).scroll(function() {
        console.log("Scroll Top: "+ $(window).scrollTop());
        // console.log("Window Height: "+ $(window).height());
        // console.log("Document Height: "+ $(document).height());
        console.log("RSVP: "+ $('#rsvp-section').offset().top);
        if($(window).scrollTop() >= $(window).height() - 90) {
            $("header").addClass("bg-dark");
        } else {
            $("header").removeClass("bg-dark");
        }

        if($(window).scrollTop() >= $('#rsvp-section').offset().top - 380) {
            $('.paper').addClass('out');
        } else {
            $('.paper').removeClass('out');
        }
    });

    $(document).on('click', '[rel=slide]', function(e) {
        e.preventDefault();
        var sel = $(this).attr('href');
        $("html, body").animate({ scrollTop: $(sel).offset().top - 80 }, 1000);
    });
});
